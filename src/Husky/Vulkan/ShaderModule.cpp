#include <Husky/Vulkan/ShaderModule.h>
#include <Husky/Vulkan/GraphicsDevice.h>

namespace Husky::Vulkan
{
    ShaderModule::ShaderModule(GraphicsDevice* aDevice, vk::ShaderModule aModule)
        : device(aDevice)
        , module(aModule)
    {
    }

    ShaderModule::~ShaderModule()
    {
        Destroy();
    }

    void ShaderModule::Destroy()
    {
        if (device)
        {
            device->DestroyShaderModule(this);
        }
    }
}
