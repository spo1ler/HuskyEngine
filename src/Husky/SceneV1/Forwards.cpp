#pragma once

namespace Husky::SceneV1
{
    class Buffer;
    class Camera;
    class IndexBuffer;
    class PbrMaterial;
    class Mesh;
    class Node;
    class OrthographicCamera;
    class PbrMaterial;
    class PerspectiveCamera;
    class Primitive;
    class Sampler;
    class Scene;
    class Texture;
    class VertexBuffer;
}
