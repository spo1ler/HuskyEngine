#pragma once

namespace Husky::SceneV1
{
    enum class CameraType
    {
        Orthographic,
        Perspective,
    };
}
