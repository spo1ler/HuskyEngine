#include <Husky/SceneV1/PbrMaterial.h>
#include <Husky/SceneV1/Texture.h>

namespace Husky::SceneV1
{
    PbrMaterial::PbrMaterial() = default;
    PbrMaterial::~PbrMaterial() = default;
}
