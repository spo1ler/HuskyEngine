#pragma once

#include <cassert>

#define HUSKY_ASSERT(cond) assert(cond)
#define HUSKY_ASSERT_MSG(cond, message) assert(cond)
