#pragma once

namespace Husky::Vulkan
{
    class Buffer;
    class BufferView;
    class CommandBuffer;
    class CommandPool;
    class DescriptorPool;
    class DescriptorSet;
    class DescriptorSetBinding;
    class DescriptorSetLayout;
    class DescriptorSetWrites;
    class Fence;
    class Framebuffer;
    class GlslShaderCompiler;
    class GraphicsDevice;
    class Image;
    class ImageView;
    class IndexBuffer;
    class PhysicalDevice;
    class Pipeline;
    class PipelineBarrier;
    class PipelineCache;
    class PipelineLayout;
    class PresentQueue;
    class Queue;
    class QueueInfo;
    class RenderPass;
    class Sampler;
    class SamplerObject;
    class Semaphore;
    class ShaderCompiler;
    class ShaderModule;
    class SubpassDescription;
    class Surface;
    class Swapchain;
    class VertexBuffer;

    struct DescriptorSetLayoutCreateInfo;
    struct FramebufferCreateInfo;
    struct GraphicsPipelineCreateInfo;
    struct RenderPassCreateInfo;
    struct PipelineLayoutCreateInfo;
    struct SubpassCreateInfo;
    struct SwapchainCreateInfo;
}