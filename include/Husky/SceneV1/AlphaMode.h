#pragma once

namespace Husky::SceneV1
{
    enum class AlphaMode
    {
        Opaque,
        Mask,
        Blend,
    };
}
